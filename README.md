Developed regression model for predicting movie revenue and IMDB ratings from 5000 movies in IMDB database.
Extracted and preprocessed 28 movie pre-release features including major casting information, genre and country etc.
Applied machine learning models, Random Forest and Decision Tree regression on obtained categorical, numerical and textual
features, and attained regression error (Mean Squared Error) of 0.005 on scale of 1.
